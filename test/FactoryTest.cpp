//
// Created by Federico Fuga on 07/03/18.
//

#include <gtest/gtest.h>

#include <Factory.h>

#include <memory>

TEST(UtilsFactory, Create)
{
    class A {
    public:
        virtual ~A()
        {}

        virtual int type() = 0;
    };
    class A1 : public A {
    public:
        int type() override
        { return 1; }
    };
    class A2 : public A {
    public:
        int type() override
        { return 2; }
    };
    class B : public A {
        int v;
    public:
        explicit B(int x)
        { v = x; }

        int type() override
        { return 2 + v; }
    };

    Factory<int, FactoryFunction<A, std::shared_ptr>> f;
    f.addFactory()
            (1, []() { return std::make_unique<A1>(); })
            (2, []() { return std::make_unique<A2>(); })
            (3, []() { return std::make_unique<B>(10); });


    std::shared_ptr<A> a;
    ASSERT_NO_THROW(a = f.create(1));
    ASSERT_EQ(a->type(), 1);
    ASSERT_NO_THROW(a = f.create(2));
    ASSERT_EQ(a->type(), 2);
    ASSERT_NO_THROW(a = f.create(3));
    ASSERT_EQ(a->type(), 12);
}

TEST(UtilsFactory, CreateWithArguments)
{
    class Op {
    public:
        virtual ~Op()
        {}

        virtual int result() = 0;
    };
    class Sum2 : public Op {
        int s;
    public:
        explicit Sum2(int x) : s(x)
        {};

        int result() override
        { return s + 2; }
    };
    class Mult2 : public Op {
    public:
        int s;
    public:
        explicit Mult2(int x) : s(x)
        {};

        int result() override
        { return s * 2; }
    };

    Factory<int, FactoryFunction<Op, std::shared_ptr, int>> f;
    f.addFactory()
            (1, [](int x) { return std::make_unique<Sum2>(x); })
            (2, [](int x) { return std::make_unique<Mult2>(x); });

    std::shared_ptr<Op> a;
    ASSERT_NO_THROW(a = f.create(1, 10));
    ASSERT_EQ(a->result(), 12);
    ASSERT_NO_THROW(a = f.create(2, 10));
    ASSERT_EQ(a->result(), 20);
}

TEST(UtilsFactory, UseRegexMatcher)
{
    class A {
    public:
        virtual ~A()
        {}

        virtual int type() = 0;
    };
    class A1 : public A {
    public:
        int type() override
        { return 1; }
    };
    class A2 : public A {
    public:
        int type() override
        { return 2; }
    };
    class B : public A {
        int v;
    public:
        explicit B(int x)
        { v = x; }

        int type() override
        { return 2 + v; }
    };

    Factory<std::regex, FactoryFunction<A, std::shared_ptr>, RegexFactoryMatcher<std::regex, std::string, FactoryFunction<A, std::shared_ptr>>> f;
    f.addFactory()
            (std::regex("[A-Z]"), []() { return std::make_unique<A1>(); })
            (std::regex("[0-9]"), []() { return std::make_unique<A2>(); })
            (std::regex("[A-Z][A-Z]+"), []() { return std::make_unique<B>(10); });


    std::shared_ptr<A> a;
    ASSERT_NO_THROW(a = f.create("A"));
    ASSERT_EQ(a->type(), 1);
    ASSERT_NO_THROW(a = f.create("2"));
    ASSERT_EQ(a->type(), 2);
    ASSERT_NO_THROW(a = f.create("AA"));
    ASSERT_EQ(a->type(), 12);
    ASSERT_THROW(a = f.create("28"), FactoryKeyNotFoundException);
}
