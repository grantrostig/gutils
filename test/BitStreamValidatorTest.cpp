/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 18/01/19
 */


#include "StrongType/BitStreamOperations.h"
#include "StrongType/BitFieldValidators.h"
#include "Enum/BitStreamOperations.h"
#include "Enum/BitFieldValidators.h"

#include <gtest/gtest.h>

using namespace utils;

TEST(UtilsBitStreamValidators, Validator)
{
    std::vector<uint8_t> d{0x25, 0x04};
    InBitStream is{d};

    struct XTag {
    };
    enum class XEnum {
        A = 1, B = 2, C = 3, D = 4,
    };
    using XE = utils::BitAwareEnum<4, XEnum, XTag>;
    struct XValidator {
        void validate(XE const &x)
        {
            if (x.underlyingValue() <= 0 || x.underlyingValue() > 4) {
                throw utils::InBitStream::IllegalValueException("Illegal XE value");
            }
        }
    };
    using X = utils::ValidatedType<XE, XValidator>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::B);

    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);
    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::D);
}

TEST(UtilsBitStreamValidators, ValidatorReadyMadeEnum)
{
    std::vector<uint8_t> d{0x25, 0x04};
    InBitStream is{d};

    struct XTag {
    };
    enum class XEnum {
        A = 1, B = 2, C = 3, D = 4,
    };
    using XE = utils::BitAwareEnum<4, XEnum, XTag>;
    using X = utils::ValidatedType<XE, utils::validators::EnumMinMaxValidator<XE, XEnum, XEnum::A, XEnum::D>>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::B);

    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);
    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::D);
}

TEST(UtilsBitStreamValidators, ValidatorReadyMadeStrongType)
{
    std::vector<uint8_t> d{0x25, 0x04};
    InBitStream is{d};

    struct XTag {
    };
    using XE = utils::BitAwareStrongType<4, int, XTag>;
    using X = utils::ValidatedType<XE, utils::validators::StrongTypeMinMaxValidator<XE, int, 1, 4>>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), 2);

    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);
    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), 4);
}

TEST(UtilsBitStreamValidators, SpecialValuesValidatorTest)
{
    std::vector<uint8_t> d{0x25, 0x37};
    InBitStream is{d};

    struct XTag {
    };
    enum class XEnum {
        A = 1, B = 2, InvalidC = 3, D = 4, InvalidE = 5, F = 6, G = 7
    };
    using XE = utils::BitAwareEnum<4, XEnum, XTag>;
    using X = utils::ValidatedType<XE, utils::validators::EnumSpecialValidator<XE, XEnum, XEnum::A, XEnum::G, XEnum::InvalidC, XEnum::InvalidE>>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::B);

    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);
    EXPECT_THROW(is >> x, InBitStream::IllegalValueException);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::G);
}