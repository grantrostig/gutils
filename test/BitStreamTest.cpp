//
// Created by Federico Fuga on 19/11/18.
//

#include "BitField.h"
#include "BitStream.h"
#include "BitFieldValidator.h"
#include "EndiannesTypes.h"

#include "StrongType/BitStreamOperations.h"
#include "Enum/BitStreamOperations.h"

#include <gtest/gtest.h>

using namespace utils;

TEST(UtilsBitStream, BitField)
{
    BitField<4, uint8_t> a{0x08};

    EXPECT_EQ(a.leftAligned(), 0x80);
    EXPECT_EQ(a.bits(), 4);

    BitField<3, uint8_t> b{0x04};
    EXPECT_EQ(b.leftAligned(), 0x80);
    EXPECT_EQ(b.bits(), 3);

    BitField<12, uint16_t> c{1};
    EXPECT_EQ(c.leftAligned(), 0x0010);
}

TEST(UtilsBitStream, BitStreamOutByte)
{
    std::vector<uint8_t> buffer;
    OutBitStream bs1(buffer);

    std::vector<uint8_t> step1{0x80};
    bs1 << uint8_t(0x80);
    EXPECT_EQ(bs1.bytes(), step1);
}

TEST(UtilsBitStream, BitStreamOut1)
{
    std::vector<uint8_t> buffer;
    OutBitStream bs1(buffer);

    std::vector<uint8_t> step1{0x80};
    bs1 << BitField<2>(2);
    EXPECT_EQ(bs1.bytes(), step1);

    bs1 << uint8_t(0x55);
    std::vector<uint8_t> step2{0x95, 0x40};
    EXPECT_EQ(bs1.bytes(), step2);

}

TEST(UtilsBitStream, BitStreamOut2)
{
    std::vector<uint8_t> buffer;
    OutBitStream bs1(buffer);

    std::vector<uint8_t> step1{0xC0};
    bs1 << BitField<2>(3);
    EXPECT_EQ(bs1.bytes(), step1);

    bs1 << BitField<14>(0x0555);

    std::vector<uint8_t> res{0xc5, 0x55};
    EXPECT_EQ(bs1.bytes(), res);

}

TEST(UtilsBitStream, BitStreamOut3)
{
    std::vector<uint8_t> buffer;
    OutBitStream bs1(buffer);

    bs1 << uint8_t(0x40) << uint8_t(0xaa);

    std::vector<uint8_t> res{0x40, 0x0aa};
    EXPECT_EQ(bs1.bytes(), res);

    BitField<5> v1{0x11};
    bs1 << v1;

    std::vector<uint8_t> res1{0x40, 0x0aa, 0b10001000};
    EXPECT_EQ(bs1.bytes(), res1);

    BitField<2> v2{0x03};
    bs1 << v2;

    std::vector<uint8_t> res2{0x40, 0x0aa, 0b10001110};
    EXPECT_EQ(bs1.bytes(), res2);

    BitField<2> v3{0x03};
    bs1 << v3;

    std::vector<uint8_t> res3{0x40, 0x0aa, 0b10001111, 0b10000000};
    EXPECT_EQ(bs1.bytes(), res3);

    BitField<12> v4{0x0a81};
    bs1 << v4;
    std::vector<uint8_t> res4{0x40, 0x0aa, 0b10001111, 0b11010100, 0b00001000};
    EXPECT_EQ(bs1.bytes(), res4);

    BitField<15> v5{0x7081};
    bs1 << v5;
    std::vector<uint8_t> res5{0x40, 0x0aa, 0b10001111, 0b11010100, 0b00001111, 0x08, 0x10};
    EXPECT_EQ(bs1.bytes(), res5);

    BitField<12> v6{0xaaa};
    bs1 << v6;
    std::vector<uint8_t> res6{0x40, 0x0aa, 0b10001111, 0b11010100, 0b00001111, 0x08, 0x1a, 0xaa};
    EXPECT_EQ(bs1.bytes(), res6);

    BitField<8> v7{0x55};
    bs1 << v7;
    std::vector<uint8_t> res7{0x40, 0x0aa, 0b10001111, 0b11010100, 0b00001111, 0x08, 0x1a, 0xaa, 0x55};
    EXPECT_EQ(bs1.bytes(), res7);
}

TEST(UtilsBitStream, BitStreamIn1)
{
    std::vector<uint8_t> data{0x86, 0x21};
    InBitStream bs{data};

    EXPECT_TRUE(!bs);
    EXPECT_FALSE(bs.atEnd());

    BitField<2> f1;
    bs >> f1;
    EXPECT_EQ(f1.value(), 0x02);

    BitField<4> f2;
    bs >> f2;
    EXPECT_EQ(f2.value(), 0x01);

    BitField<6> f3;
    bs >> f3;
    EXPECT_EQ(f3.value(), 0x22);

    BitField<4> f4;
    bs >> f4;
    EXPECT_EQ(f4.value(), 0x01);

    EXPECT_FALSE(!bs);
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, BitStreamIn2)
{
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};

    EXPECT_TRUE(!bs);
    EXPECT_FALSE(bs.atEnd());

    BitField<7> f1;
    bs >> f1;
    EXPECT_EQ(f1.value(), 0x43);

    BitField<11> f2;
    bs >> f2;
    EXPECT_EQ(f2.value(), 0x485);

    BitField<6> f3;
    bs >> f3;
    EXPECT_EQ(f3.value(), 0x15);

    EXPECT_FALSE(!bs);
    EXPECT_TRUE(bs.atEnd());

    EXPECT_THROW(bs >> f3, InBitStream::InputPastEndOverflow);
}

TEST(UtilsBitStream, InStrongType)
{
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};

    struct Tag {
    };
    utils::StrongType<uint8_t, Tag> st;

    BitField<8, decltype(st) &> field(st);

    bs >> field;

    EXPECT_EQ(st.value(), 0x87);

    utils::StrongType<uint16_t, Tag> st2;
    BitField<12, decltype(st2) &> field2(st2);
    bs >> field2;

    EXPECT_EQ(st2.value(), 0x215);

    utils::StrongType<uint16_t, Tag> st3;
    BitField<3, decltype(st3) &> field3(st3);
    bs >> field3;

    EXPECT_EQ(st3.value(), 0x2);

    utils::StrongType<int, Tag> st4;
    BitField<1, decltype(st4) &> field4(st4);
    bs >> field4;

    EXPECT_EQ(st4.value(), 0x1);
}

TEST(UtilsBitStream, OutStrongType)
{
    std::vector<uint8_t> data;
    OutBitStream bs{data};

    struct Tag {
    };
    utils::StrongType<uint8_t, Tag> st{0x87};

    BitField<8, decltype(st) &> field(st);

    bs << field;

    std::vector<uint8_t> r1{0x87};
    EXPECT_EQ(bs.bytes(), r1);

    utils::StrongType<uint16_t, Tag> st2{0x215};
    BitField<12, decltype(st2) &> field2(st2);
    bs << field2;

    std::vector<uint8_t> r2{0x87, 0x21, 0x50};
    EXPECT_EQ(bs.bytes(), r2);

    utils::StrongType<uint16_t, Tag> st3{0x2};
    BitField<3, decltype(st3) &> field3(st3);
    bs << field3;

    std::vector<uint8_t> r3{0x87, 0x21, 0x54};
    EXPECT_EQ(bs.bytes(), r3);

    utils::StrongType<int, Tag> st4{1};
    BitField<1, decltype(st4) &> field4(st4);
    bs << field4;

    std::vector<uint8_t> r4{0x87, 0x21, 0x55};
    EXPECT_EQ(bs.bytes(), r4);
}

TEST(UtilsBitStream, InBoolean)
{
    std::vector<uint8_t> data{0xa5};
    InBitStream bs{data};

    bool a, b, c, d, e, f;
    bs >> a >> b >> c >> d >> e >> f;

    EXPECT_EQ(a, true);
    EXPECT_EQ(b, false);
    EXPECT_EQ(c, true);
    EXPECT_EQ(d, false);
    EXPECT_EQ(e, false);
    EXPECT_EQ(f, true);
}

TEST(UtilsBitStream, OutBoolean)
{
    std::vector<uint8_t> data;
    OutBitStream bs{data};

    bs << true;
    std::vector<uint8_t> a{0x80};
    EXPECT_EQ(bs.bytes(), a);

    bs << false;
    a = std::vector<uint8_t>{0x80};
    EXPECT_EQ(bs.bytes(), a);

    bs << true;
    a = std::vector<uint8_t>{0xa0};
    EXPECT_EQ(bs.bytes(), a);

    bs << false;
    a = std::vector<uint8_t>{0xa0};
    EXPECT_EQ(bs.bytes(), a);

    bs << false;
    a = std::vector<uint8_t>{0xa0};
    EXPECT_EQ(bs.bytes(), a);

    bs << true;
    a = std::vector<uint8_t>{0xa4};
    EXPECT_EQ(bs.bytes(), a);

    bs << false;
    a = std::vector<uint8_t>{0xa4};
    EXPECT_EQ(bs.bytes(), a);

    bs << true;
    a = std::vector<uint8_t>{0xa5};
    EXPECT_EQ(bs.bytes(), a);
}

TEST(UtilsBitStream, InRef)
{
    struct Tag {
    };

    std::vector<uint8_t> data{0xa5, 0x55};
    InBitStream bs{data};

    int x, y;
    utils::StrongType<uint8_t, Tag> z;

    auto f1 = makeBitFieldRef<8>(x);
    auto f2 = makeBitFieldRef<4>(y);
    auto f3 = makeBitFieldRef<4>(z);

    ASSERT_NO_THROW(bs >> f1 >> f2 >> f3);
    EXPECT_EQ(x, 0xa5);
    EXPECT_EQ(y, 0x05);
    EXPECT_EQ(z.value(), 0x05);
}

TEST(UtilsBitStream, InRefRValue)
{
    struct Tag {
    };

    std::vector<uint8_t> data{0xa5, 0x55};
    InBitStream bs{data};

    int x, y;
    utils::StrongType<uint8_t, Tag> z;

    bs >> BitField<8, int &>(x);
    bs >> makeBitFieldRef<4>(y);
    bs >> makeBitFieldRef<4>(z);

    EXPECT_EQ(x, 0xa5);
    EXPECT_EQ(y, 0x05);
    EXPECT_EQ(z.value(), 0x05);
}

TEST(UtilsBitStream, OutRef)
{
    struct Tag {
    };

    std::vector<uint8_t> data{};
    std::vector<uint8_t> expected{0xa5, 0x87};
    OutBitStream bs{data};

    int x = 0xa5, y = 0x08;
    utils::StrongType<uint8_t, Tag> z{0x07};

    auto f1 = makeBitFieldRef<8>(x);
    auto f2 = makeBitFieldRef<4>(y);
    auto f3 = makeBitFieldRef<4>(z);

    ASSERT_NO_THROW(bs << f1 << f2 << f3);

    EXPECT_EQ(data, expected);
}

TEST(UtilsBitStream, OutRefRValue)
{
    struct Tag {
    };

    std::vector<uint8_t> data{};
    std::vector<uint8_t> expected{0xa5, 0x87};
    OutBitStream bs{data};

    int x = 0xa5, y = 0x08;
    utils::StrongType<uint8_t, Tag> z{0x07};

    bs << BitField<8, int &>(x);
    bs << makeBitFieldRef<4>(y);
    bs << makeBitFieldRef<4>(z);

    EXPECT_EQ(data, expected);
}

TEST(UtilsBitStream, OutRefRValueConst)
{
    struct Tag {
    };

    std::vector<uint8_t> data{};
    std::vector<uint8_t> expected{0xa5, 0x87};
    OutBitStream bs{data};

    const int x = 0xa5, y = 0x08;
    const utils::StrongType<uint8_t, Tag> z{0x07};

    bs << BitField<8, const int &>(x);
    bs << makeBitFieldRef<4>(y);
    bs << makeBitField<4>(z);

    EXPECT_EQ(data, expected);
}

TEST(UtilsBitStream, InEnum)
{
    enum class E {
        A = 0, B = 1, C = 2, D = 3
    };

    std::vector<uint8_t> data{0x30};
    InBitStream bs{data};

    E e;
    auto efield = makeBitFieldRef<4>(e);

    bs >> efield;

    EXPECT_EQ(e, E::D);
}

TEST(UtilsBitStream, OutEnum)
{
    enum class E {
        A = 0, B = 1, C = 2, D = 3
    };

    std::vector<uint8_t> data{};
    std::vector<uint8_t> expected{0x20};
    OutBitStream bs{data};

    E e = E::C;
    auto efield = makeBitFieldRef<4>(e);

    bs << efield;

    EXPECT_EQ(data, expected);
}

TEST (UtilsBitStream, outStrongTypes)
{
    struct X {
    };
    using XType = utils::StrongType<int, X>;
    XType x(0x80);

    std::vector<uint8_t> data;
    OutBitStream outs(data);

    outs << makeBitField<8>(x);
    outs << makeBitFieldRef<8>(x);

    EXPECT_EQ(data, (std::vector<uint8_t>{0x80, 0x80}));
}

TEST(UtilsBitStream, InBitStreamReadReamining1)
{
    // Read all the data
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};

    std::vector<uint8_t> v;
    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, data);
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, InBitStreamReadReamining2)
{
    // Skip the first byte, read the reamining
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};
    bs.skipUnaligned();

    int x;
    ASSERT_NO_THROW(bs >> makeBitFieldRef<1>(x));

    std::vector<uint8_t> v;
    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, (std::vector<uint8_t>{0x21, 0x55}));
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, InBitStreamReadReamining2Fail)
{
    // Test unaligned read
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};

    int x;
    ASSERT_NO_THROW(bs >> makeBitFieldRef<1>(x));

    ASSERT_THROW(bs.remainingBytes(), std::runtime_error);      // unaligned read
}

TEST(UtilsBitStream, InBitStreamReadReamining3)
{
    // Skip the first byte, read the reamining, same as above, but read 8 bits
    std::vector<uint8_t> data{0x87, 0x21, 0x55};
    InBitStream bs{data};

    int x;
    ASSERT_NO_THROW(bs >> makeBitFieldRef<8>(x));

    std::vector<uint8_t> v;
    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, (std::vector<uint8_t>{0x21, 0x55}));
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, InBitStreamReadReamining4)
{
    // Read first byte, return an empty array
    std::vector<uint8_t> data{0x87};
    InBitStream bs{data};

    int x;
    ASSERT_NO_THROW(bs >> makeBitFieldRef<8>(x));

    std::vector<uint8_t> v;
    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, (std::vector<uint8_t>{}));
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, InBitStreamReadReamining5)
{
    // Read from an empty array, and reread
    std::vector<uint8_t> data{};
    InBitStream bs{data};

    std::vector<uint8_t> v;
    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, (std::vector<uint8_t>{}));
    EXPECT_TRUE(bs.atEnd());

    ASSERT_NO_THROW(v = bs.remainingBytes());

    EXPECT_EQ(v, (std::vector<uint8_t>{}));
    EXPECT_TRUE(bs.atEnd());
}

TEST(UtilsBitStream, InBitStreamPick)
{
    std::vector<uint8_t> data{0x01, 0x52, 0x03};
    InBitStream bs(data);

    uint8_t v, v2;
    EXPECT_NO_THROW(bs.pick(v));
    EXPECT_EQ(v, 0x01);
    bs >> v2;
    EXPECT_EQ(v2, v);

    EXPECT_NO_THROW(bs.pick(utils::makeBitFieldRef<4>(v)));
    EXPECT_EQ(v, 0x05);
    bs >> utils::makeBitFieldRef<4>(v2);
    EXPECT_EQ(v2, v);

    uint16_t w, w2;
    EXPECT_NO_THROW(bs.pick(utils::makeBitFieldRef<12>(w)));
    EXPECT_EQ(w, 0x203);
    bs >> utils::makeBitFieldRef<12>(w2);
    EXPECT_EQ(w2, w);
}

TEST(UtilsBitStream, OutBitStreamWriteArray)
{
    // skip to the next byte if write is not aligned
    std::vector<uint8_t> data;

    OutBitStream bs{data};
    bs.skipUnaligned();

    ASSERT_NO_THROW(bs << makeBitField<1>(0));

    std::vector<uint8_t> v{0x21, 0x55};
    ASSERT_NO_THROW(bs << v);

    EXPECT_EQ(data, (std::vector<uint8_t>{0x00, 0x21, 0x55}));
}

TEST(UtilsBitStream, OutBitStreamWriteArrayFail)
{
    // fail if the write position is not aligned
    std::vector<uint8_t> data;

    OutBitStream bs{data};

    ASSERT_NO_THROW(bs << makeBitField<1>(0));

    std::vector<uint8_t> v{0x21, 0x55};
    ASSERT_THROW(bs << v, std::runtime_error);
}

TEST(UtilsBitStream, OutBitStreamWriteArrayAligned)
{
    // skip to the next byte if write is not aligned
    std::vector<uint8_t> data;

    OutBitStream bs{data};

    ASSERT_NO_THROW(bs << makeBitField<8>(0));

    std::vector<uint8_t> v{0x21, 0x55};
    ASSERT_NO_THROW(bs << v);

    EXPECT_EQ(data, (std::vector<uint8_t>{0x00, 0x21, 0x55}));
}

TEST(UtilsBitStream, InBitAwareTypes)
{
    std::vector<uint8_t> d{0xf0, 0x0f};
    InBitStream is{d};

    struct XTag {
    };
    using X = utils::BitAwareStrongType<3, int, XTag>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), 0x7);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), 0x4);
}

TEST(UtilsBitStream, OutBitAwareTypes)
{
    std::vector<uint8_t> out;
    std::vector<uint8_t> exp{0xea};
    OutBitStream is{out};

    struct XTag {
    };
    struct YTag {
    };
    using X = utils::BitAwareStrongType<3, int, XTag>;
    using Y = utils::BitAwareStrongType<2, int, YTag>;

    X x{0x7};
    Y y{0x1};
    X z{0x2};

    EXPECT_NO_THROW(is << x);
    EXPECT_NO_THROW(is << y);
    EXPECT_NO_THROW(is << z);
    EXPECT_EQ(out, exp);
}

TEST(UtilsBitStream, InStrongEnum)
{
    std::vector<uint8_t> d{0xf0, 0x0f};
    InBitStream is{d};

    struct XTag {
    };
    enum class XEnum {
        A = 1, B = 2, C = 3, D = 4, E = 5, F = 6, G = 7
    };
    using X = utils::BitAwareEnum<3, XEnum, XTag>;
    X x;

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::G);

    EXPECT_NO_THROW(is >> x);
    EXPECT_EQ(x.value(), X::Enums::D);
}

TEST(UtilsBitStream, OutStrongEnum)
{
    std::vector<uint8_t> out;
    std::vector<uint8_t> exp{0xe5, 0x00};
    OutBitStream is{out};

    struct XTag {
    };
    enum class XEnum {
        A = 1, B = 2, C = 3, D = 4, E = 5, F = 6, G = 7
    };
    using X = utils::BitAwareEnum<3, XEnum, XTag>;

    X x{XEnum::G};
    X y{XEnum::A};
    X z{XEnum::B};

    EXPECT_NO_THROW(is << x);
    EXPECT_NO_THROW(is << y);
    EXPECT_NO_THROW(is << z);
    EXPECT_EQ(out, exp);
}

TEST(UtilsBitStream, Reserved)
{
    std::vector<uint8_t> out;
    std::vector<uint8_t> exp{0x00};
    OutBitStream outs{out};

    EXPECT_NO_THROW(outs << utils::ReservedBits<3>());
    EXPECT_NO_THROW(outs << utils::ReservedBits<2>());
    EXPECT_NO_THROW(outs << utils::ReservedBits<3>());
    EXPECT_EQ(out, exp);

    std::vector<uint8_t> src{0x10};
    InBitStream ins{src};
    EXPECT_NO_THROW(ins >> utils::ReservedBits<3>());
    EXPECT_THROW(ins >> utils::ReservedBits<2>(), InBitStream::IllegalValueException);
    EXPECT_NO_THROW(ins >> utils::ReservedBits<3>());
}
