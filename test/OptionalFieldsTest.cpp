//
// Created by Federico Fuga on 05/06/18.
//

#include "OptionalFields.h"
#include "DataStream.h"
#include "DataStreamCommons.h"

#include <gtest/gtest.h>

using namespace utils;

TEST(UtilsOptionalFields, values)
{
    OptionalField<int, 0> intv;

    ASSERT_FALSE(intv.isSet());
    ASSERT_THROW(intv.value(), boost::bad_optional_access);

    intv = 1;

    ASSERT_TRUE(intv.isSet());
    ASSERT_EQ(intv.value(), 1);

    ASSERT_NO_THROW(intv.unset());
    ASSERT_FALSE(intv.isSet());
    ASSERT_THROW(intv.value(), boost::bad_optional_access);
}

TEST(UtilsOptionalFields, bitmap)
{
    OptionalField<std::string, 0> first;
    OptionalField<int, 1> second;
    OptionalField<int, 2> third;
    OptionalField<double, 3> fourth;

    OptionalFieldBitmap<4> bitmap;

    ASSERT_EQ(bitmap.size(), 1);

    ASSERT_EQ(OptionalFieldBitmap<8>().size(), 1);
    ASSERT_EQ(OptionalFieldBitmap<9>().size(), 2);
    ASSERT_EQ(OptionalFieldBitmap<16>().size(), 2);

    std::string stringValue{"abracadabra"};
    int intValue = 100;

    ASSERT_TRUE(first.isUnset());
    ASSERT_FALSE(first.isSet());

    first = stringValue;

    ASSERT_FALSE(first.isUnset());
    ASSERT_TRUE(first.isSet());
    ASSERT_EQ(first.value(), stringValue);

    ASSERT_FALSE(second.isSet());
    second = intValue;
    ASSERT_TRUE(second.isSet());
    ASSERT_EQ(second.value(), intValue);

    bitmap.assign(first);
    bitmap.assign(second);
    bitmap.assign(third);
    bitmap.assign(fourth);

    ASSERT_TRUE (bitmap.isSet(first));
    ASSERT_TRUE (bitmap.isSet(second));
    ASSERT_FALSE (bitmap.isSet(third));
    ASSERT_FALSE (bitmap.isSet(fourth));

    ASSERT_EQ(bitmap.get(0), 0x03);
}

TEST(UtilsOptionalFields, stream)
{
    OptionalField<std::string, 0> first;
    OptionalField<int32_t, 1> second;
    OptionalField<int8_t, 2> third;
    struct FourthTag {
    };
    OptionalField<utils::StrongType<int, FourthTag>, 3> fourth;

    first = "AA";
    third = 1;

    OptionalFieldBitmap<4> bitmap{first, second, third, fourth};

    ASSERT_TRUE (bitmap.isSet(first));
    ASSERT_FALSE (bitmap.isSet(second));
    ASSERT_TRUE (bitmap.isSet(third));
    ASSERT_FALSE (bitmap.isSet(fourth));

    std::vector<uint8_t> exp{0x05, 0x02, 'A', 'A', 0x01};
    std::vector<uint8_t> data;
    OutputDataStream<BigEndian> ds(data);

    ds << bitmap;
    ds << first << second << third << fourth;

    ASSERT_EQ(data, exp);

    InputDataStream<BigEndian> is(data);
    OptionalFieldBitmap<4> inbitmap{first, second, third, fourth};

    inbitmap.read(is) >> first >> second >> third >> fourth;

    ASSERT_TRUE (inbitmap.isSet(first));
    ASSERT_FALSE (inbitmap.isSet(second));
    ASSERT_TRUE (inbitmap.isSet(third));
    ASSERT_FALSE (inbitmap.isSet(fourth));

    ASSERT_TRUE(first.isSet());
    ASSERT_EQ(first.value(), "AA");
    ASSERT_FALSE(second.isSet());
    ASSERT_TRUE(third.isSet());
    ASSERT_EQ(third.value(), 1);
    ASSERT_FALSE(fourth.isSet());

    OptionalField<std::string, 0> v0;
    OptionalField<int32_t, 1> v1;
    OptionalField<int32_t, 2> v2;
    OptionalField<int32_t, 3> v3;
    OptionalField<int32_t, 4> v4;
    OptionalField<int32_t, 5> v5;
    OptionalField<int32_t, 6> v6;
    OptionalField<int32_t, 7> v7;
    OptionalField<int32_t, 8> v8;
    OptionalField<int32_t, 9> v9;
    OptionalField<int32_t, 10> v10;
    OptionalField<int32_t, 11> v11;
    OptionalField<int32_t, 12> v12;
    OptionalField<int32_t, 13> v13;
    OptionalField<int32_t, 14> v14;
    OptionalField<int32_t, 15> v15;

    std::vector<uint8_t> exp2{0x01, 0x01, 0x02, 'A', 'A', 0x00, 0x00, 0x00, 0x01};
    InputDataStream<BigEndian> is2(exp2);
    OptionalFieldBitmap<16> inbitmap2{v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15};

    inbitmap2.read(is2) >> v0 >> v1 >> v2 >> v3 >> v4 >> v5 >> v6 >> v7 >> v8 >> v9 >> v10 >> v11 >> v12 >> v13 >> v14
                        >> v15;
    ASSERT_TRUE(v0.isSet());
    ASSERT_TRUE(v8.isSet());
    ASSERT_EQ(v0.value(), "AA");
    ASSERT_EQ(v8.value(), 1);
}
