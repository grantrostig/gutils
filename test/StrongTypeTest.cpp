//
// Created by happycactus on 01/10/18.
//

#include "StrongType.h"
#include "StrongType/BitStreamTypes.h"

#include <gtest/gtest.h>

#include <type_traits>


#define STATIC_ASSERT(expr) static_assert(expr, #expr)

TEST(UtilsStrongTypeTest, SetGetValue)
{
    struct ITag {
    };
    using I = utils::StrongType<int, ITag>;

    I i{120};

    EXPECT_EQ(i.value(), 120);

    I x{200};

    int y = 300;

    STATIC_ASSERT((std::is_assignable<decltype(i), decltype(x)>::value));
    STATIC_ASSERT((!std::is_assignable<decltype(i), decltype(y)>::value));
}

TEST(UtilsStrongTypeTest, Assign)
{
    struct STag {
    };
    struct ITag {
    };

    using S = utils::StrongType<std::string, STag>;
    using I = utils::StrongType<std::string, ITag>;

    STATIC_ASSERT((!std::is_assignable<I, S>::value));

    S s{"Abracadabra"};
    I i{s.value()};

    auto x = I{s.value()};

    struct NTag {
    };
    using N = utils::StrongType<int, NTag>;
    using NBit = utils::BitAwareStrongType<2, int, NTag>;

    NBit n;
    n = N{10};
}

TEST(UtilsStrongTypeTest, RelationalOperators)
{
    struct ITag {
    };
    using I = utils::StrongType<int, ITag>;

    EXPECT_TRUE(I{0} < I{1});
    EXPECT_TRUE(I{0} <= I{1});
    EXPECT_TRUE(I{1} <= I{1});
    EXPECT_TRUE(I{10} > I{1});
    EXPECT_TRUE(I{10} >= I{1});
    EXPECT_TRUE(I{10} >= I{10});
    EXPECT_TRUE(I{1} == I{1});
    EXPECT_TRUE(I{3} != I{1});
}

TEST(UtilsStrongTypeTest, ArithmeticOperations)
{
    struct ITag {
    };
    using I = utils::StrongType<int, ITag>;

    I i1{10};
    I i2{5};

    EXPECT_EQ(-i1, I{-10});
    EXPECT_EQ(i1 + i2, I{15});
    EXPECT_EQ(i1 - i2, I{5});
    EXPECT_EQ(i1 * i2, I{50});
    EXPECT_EQ(i1 / i2, I{2});
    EXPECT_EQ(i1 % i2, I{0});
}

TEST(UtilsStrongTypeTest, ArithmeticOperationsWithValidation)
{
    struct ITag {
    };
    using IV = utils::StrongType<int, ITag, utils::validators::MinMaxValidator<int, 5, 10>>;
    IV i1{10};
    IV i2{6};

    EXPECT_THROW(-i1, std::out_of_range);
    EXPECT_THROW(i1 + i2, std::out_of_range);
    EXPECT_THROW(i1 - i2, std::out_of_range);
    EXPECT_THROW(i1 * i2, std::out_of_range);
    EXPECT_THROW(i1 / i2, std::out_of_range);
    EXPECT_THROW(i1 % i2, std::out_of_range);
}
