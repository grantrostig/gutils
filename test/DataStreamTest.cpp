/*
Copyright (C) 2017 Federico Fuga <fuga@studiofuga.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DataStream.h"
#include "DataStreamCommons.h"

#include <boost/optional.hpp>
#include <gtest/gtest.h>

using namespace utils;

namespace {
struct MyStruct {
    int32_t i32;
    std::string s;
    std::vector<uint8_t> v;
};

template<typename Endianness>
OutputDataStream<Endianness> &operator<<(OutputDataStream<Endianness> &stream, const MyStruct &m)
{
    stream << m.i32 << m.s << m.v;
    return stream;
}

template<typename Endianness>
InputDataStream<Endianness> &operator>>(InputDataStream<Endianness> &stream, MyStruct &m)
{
    stream >> m.i32 >> m.s >> m.v;
    return stream;
}

enum class MyEnumClass : int8_t {
    First = 0, Second, Third, Fourth
};

}

TEST(UtilsSerialization, InputDataStreamBigEndian)
{
    // Test the tDataStream class. All of these packets are good.

    std::vector<uint8_t> msg{0x01, 0x23, 0x03, 0x04, 0x80, 0x10, 0x20, 0xff};

    {
        InputDataStream<BigEndian> strm(msg);
        ASSERT_EQ(strm.get<uint8_t>(), uint8_t(0x01));
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x2303));
        ASSERT_EQ(strm.get<uint8_t>(), uint8_t(0x04));
        ASSERT_EQ(strm.get<int8_t>(), -128);
    }

    {
        InputDataStream<BigEndian> strm(msg);
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x0123));
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x0304));
        ASSERT_EQ(strm.get<uint8_t>(), 128);
    }

    {
        InputDataStream<BigEndian> strm(msg);
        ASSERT_EQ(strm.get<int32_t>(), int32_t(0x01230304));
        ASSERT_EQ(strm.get<uint32_t>(), uint32_t(0x801020ff));
    }

    {
        InputDataStream<BigEndian> strm(msg);
        ASSERT_EQ(strm.get<uint64_t>(), uint64_t(0x01230304801020ff));
    }

    {
        InputDataStream<BigEndian> strm(msg);
        ASSERT_EQ(strm.get<int64_t>(), int64_t(0x01230304801020ff));
    }

    {
        InputDataStream<BigEndian> strm(msg);
        std::array<uint8_t, 5> res{0x01, 0x23, 0x03, 0x04, 0x80};
        auto v = strm.getArray<5>();
        ASSERT_EQ(v, res);
    }

    {
        InputDataStream<BigEndian> strm(msg);
        auto v1 = strm.get<uint8_t>();
        ASSERT_EQ(v1, 0x01);
        auto v2 = strm.getAll();
        std::vector<uint8_t> r2;
        std::copy(msg.begin() + 1, msg.end(), std::back_inserter(r2));
        ASSERT_EQ(v2, r2);
    }
}

TEST(UtilsSerialization, OutputDataStreamBigEndian)
{
    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(uint16_t(0x1020));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x10, 0x20}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(int16_t(-2));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xff, 0xfe}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(uint32_t(0x10203040));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x10, 0x20, 0x30, 0x40}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(int32_t(-3));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xff, 0xff, 0xff, 0xfd}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(uint64_t(0x0102030405060708));
        ASSERT_EQ(pkt, std::vector<uint8_t>({1, 2, 3, 4, 5, 6, 7, 8}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(int64_t(-3));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd}));
    }


    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        std::vector<uint8_t> v{0x01, 0x02, 0x03, 0x04};
        strm.push(v.begin(), v.end());
        ASSERT_EQ(pkt, v);
    }

}


TEST(UtilsSerialization, InputDataStreamLittleEndian)
{
    // Test the DataStream class. All of these packets are good.

    std::vector<uint8_t> msg{0x01, 0x23, 0x03, 0x04, 0x80, 0x10, 0x20, 0xff};

    {
        InputDataStream<LittleEndian> strm(msg);
        ASSERT_EQ(strm.get<uint8_t>(), uint8_t(0x01));
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x0323));
        ASSERT_EQ(strm.get<uint8_t>(), uint8_t(0x04));
        ASSERT_EQ(strm.get<int8_t>(), -128);
    }

    {
        InputDataStream<LittleEndian> strm(msg);
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x2301));
        ASSERT_EQ(strm.get<uint16_t>(), uint16_t(0x0403));
        ASSERT_EQ(strm.get<uint8_t>(), 128);
    }

    {
        InputDataStream<LittleEndian> strm(msg);
        ASSERT_EQ(strm.get<int32_t>(), int32_t(0x04032301));
        ASSERT_EQ(strm.get<uint32_t>(), uint32_t(0xff201080));
    }

    {
        InputDataStream<LittleEndian> strm(msg);
        ASSERT_EQ(strm.get<int64_t>(), int64_t(0xff20108004032301));
    }

    {
        InputDataStream<LittleEndian> strm(msg);
        std::array<uint8_t, 5> res{0x01, 0x23, 0x03, 0x04, 0x80};
        auto v = strm.getArray<5>();
        ASSERT_EQ(v, res);
    }

    {
        InputDataStream<LittleEndian> strm(msg);
        auto v1 = strm.get<uint8_t>();
        ASSERT_EQ(v1, 0x01);
        auto v2 = strm.getAll();
        std::vector<uint8_t> r2;
        std::copy(msg.begin() + 1, msg.end(), std::back_inserter(r2));
        ASSERT_EQ(v2, r2);
    }
}

TEST(UtilsSerialization, OutputDataStreamLittleEndian)
{
    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(uint16_t(0x1020));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x20, 0x10}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(int16_t(-2));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xfe, 0xff}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(uint32_t(0x10203040));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x40, 0x30, 0x20, 0x10}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(int32_t(-3));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xfd, 0xff, 0xff, 0xff}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(uint64_t(0x0102030405060708));
        ASSERT_EQ(pkt, std::vector<uint8_t>({8, 7, 6, 5, 4, 3, 2, 1}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(int64_t(-2));
        ASSERT_EQ(pkt, std::vector<uint8_t>({0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        std::vector<uint8_t> v{0x04, 0x03, 0x02, 0x01};
        strm.push(v.begin(), v.end());
        ASSERT_EQ(pkt, v);
    }

}


TEST(UtilsSerialization, StringsOut)
{
    std::string testString{"ABCD"};

    std::vector<uint8_t> v1;
    OutputDataStream<BigEndian> be1(v1);
    be1.putString<uint8_t>(testString);

    ASSERT_EQ(std::vector<uint8_t>({0x04, 0x41, 0x42, 0x43, 0x44}), v1);

    std::vector<uint8_t> v2;
    OutputDataStream<BigEndian> be2(v2);
    be2.putString<uint16_t>(testString);
    ASSERT_EQ(std::vector<uint8_t>({0x00, 0x04, 0x41, 0x42, 0x43, 0x44}), v2);

    std::vector<uint8_t> v4;
    OutputDataStream<BigEndian> be4(v4);
    be4.putString<uint32_t>(testString);
    ASSERT_EQ(std::vector<uint8_t>({0x00, 0x00, 0x00, 0x04, 0x41, 0x42, 0x43, 0x44}), v4);

    std::vector<uint8_t> v4l;
    OutputDataStream<LittleEndian> le4(v4l);
    le4.putString<uint32_t>(testString);
    ASSERT_EQ(std::vector<uint8_t>({0x04, 0x00, 0x00, 0x00, 0x41, 0x42, 0x43, 0x44}), v4l);
}

TEST(UtilsSerialization, StringsIn)
{
    std::string testString{"ABCD"};

    std::vector<uint8_t> p1({0x04, 0x41, 0x42, 0x43, 0x44});
    InputDataStream<BigEndian> be1(p1);
    ASSERT_EQ(be1.getString<uint8_t>(), testString);

    std::vector<uint8_t> p2({0x00, 0x04, 0x41, 0x42, 0x43, 0x44});
    InputDataStream<BigEndian> be2(p2);
    ASSERT_EQ(be2.getString<uint16_t>(), testString);

    std::vector<uint8_t> p4({0x00, 0x00, 0x00, 0x04, 0x41, 0x42, 0x43, 0x44});
    InputDataStream<BigEndian> be4(p4);
    ASSERT_EQ(be4.getString<uint32_t>(), testString);

    std::vector<uint8_t> p4l({0x04, 0x00, 0x00, 0x00, 0x41, 0x42, 0x43, 0x44});
    InputDataStream<LittleEndian> le4(p4l);
    ASSERT_EQ(le4.getString<uint32_t>(), testString);

    std::vector<uint8_t> p5{0x41, 0x42, 0x43};
    InputDataStream<LittleEndian> le5(p5);
    ASSERT_THROW(le5.getString<uint8_t>(), StreamOutOfBoundException);
}

TEST(UtilsSerialization, boolean)
{
    {
        std::vector<uint8_t> pkt;
        OutputDataStream<LittleEndian> strm(pkt);
        strm.put(true);
        strm.put(false);
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x01, 0x00}));
    }

    {
        std::vector<uint8_t> pkt;
        OutputDataStream<BigEndian> strm(pkt);
        strm.put(true);
        strm.put(false);
        ASSERT_EQ(pkt, std::vector<uint8_t>({0x01, 0x00}));
    }

    std::vector<uint8_t> p({0x00, 0x01});
    InputDataStream<BigEndian> be(p);
    ASSERT_EQ(be.get<bool>(), false);
    ASSERT_EQ(be.get<bool>(), true);

    InputDataStream<LittleEndian> le(p);
    ASSERT_EQ(le.get<bool>(), false);
    ASSERT_EQ(le.get<bool>(), true);
}

TEST(UtilsSerialization, StreamOperators)
{
    std::vector<uint8_t> exp{
            0x10, 0xfb, 0xff, 0xff, 0x00, 0x00, 0x00, 0x01,
            0x03, 'A', 'B', 'C'
    };

    std::vector<uint8_t> data;
    OutputDataStream<BigEndian> stream(data);

    stream << uint8_t(0x10);
    stream << int8_t(-5) << int16_t(-1) << uint32_t(0x01);
    stream << std::string{"ABC"};

    ASSERT_EQ(exp, data);

    InputDataStream<BigEndian> inStream(exp);

    uint8_t u8;
    inStream >> u8;
    ASSERT_EQ(u8, 0x10);

    int8_t i8;
    inStream >> i8;
    ASSERT_EQ(i8, -5);

    int16_t i16;
    inStream >> i16;
    ASSERT_EQ(i16, -1);

    uint32_t u32;
    inStream >> u32;
    ASSERT_EQ(u32, 1);

    std::string s;
    inStream >> s;
    ASSERT_EQ(s, "ABC");

}

TEST(UtilsSerialization, MyStructStreamOperators)
{
    std::vector<uint8_t> exp{
            0xff, 0xff, 0xff, 0xfe,
            0x03, 'A', 'B', 'C',
            0x00, 0x02, 0x00, 0x01
    };

    InputDataStream<BigEndian> inStream(exp);
    MyStruct my;

    inStream >> my;

    ASSERT_EQ(my.i32, -2);
    ASSERT_EQ(my.s, "ABC");
    std::vector<uint8_t> expv{0x00, 0x01};
    ASSERT_EQ(my.v, expv);

    std::vector<uint8_t> out;
    OutputDataStream<BigEndian> outStream(out);

    outStream << my;

    ASSERT_EQ(exp, out);
}

TEST(UtilsSerialization, EnumClasses)
{
    std::vector<uint8_t> exp{
            0x00, 0x01, 0x03, 0x02
    };

    InputDataStream<BigEndian> is(exp);

    MyEnumClass a, b, c, d;

    is >> a >> b >> c >> d;
    ASSERT_EQ(a, MyEnumClass::First);
    ASSERT_EQ(b, MyEnumClass::Second);
    ASSERT_EQ(c, MyEnumClass::Fourth);        // note the sequence
    ASSERT_EQ(d, MyEnumClass::Third);

    std::vector<uint8_t> out;
    OutputDataStream<BigEndian> os(out);

    os << a << b << c << d;

    ASSERT_EQ(out, exp);
}

TEST(UtilsSerialization, Enums)
{
    enum X : uint8_t {
        A = 0, B = 1, C = 2
    };
    std::vector<uint8_t> exp{
            0x00, 0x01, 0x02
    };

    InputDataStream<BigEndian> is(exp);

    X a, b, c;

    is >> a >> b >> c;
    ASSERT_EQ(a, A);
    ASSERT_EQ(b, B);
    ASSERT_EQ(c, C);

    std::vector<uint8_t> out;
    OutputDataStream<BigEndian> os(out);

    os << a << b << c;

    ASSERT_EQ(out, exp);
}

TEST(UtilsSerialization, Pairs)
{
    std::pair<uint16_t, std::string> pair;

    std::vector<uint8_t> pkt{0x00, 0x01, 0x02, 'A', 'B'};

    InputDataStream<BigEndian> is(pkt);

    is >> pair;
    ASSERT_EQ(pair.first, 1);
    ASSERT_EQ(pair.second, "AB");

    std::vector<uint8_t> out;
    OutputDataStream<BigEndian> os(out);

    os << pair;

    ASSERT_EQ(out, pkt);
}
