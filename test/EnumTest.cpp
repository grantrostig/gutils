/** @file
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#include "Enum.h"
#include "Enum/BitStreamTypes.h"

#include <gtest/gtest.h>

#include <type_traits>

#define STATIC_ASSERT(expr) static_assert(expr, #expr)


enum V {
    A = 1, B
};

struct XTag {
};
using X = utils::Enum<V, XTag>;

struct YTag {
};
using Y = utils::Enum<V, YTag>;

TEST(UtilsEnum, GetSet)
{
    V va{V::A};
    X xa{X::Enums::A};
    Y ya{Y::Enums::A};
    X xb;

    // cannot assign Enums even if they comes from the same enum type
    STATIC_ASSERT((!std::is_assignable<decltype(xa), decltype(ya)>::value));

    // but can assign Enums of the same type
    STATIC_ASSERT((std::is_assignable<decltype(xa), decltype(xb)>::value));

    // Cannot convert Enums of other types
    STATIC_ASSERT((!std::is_convertible<decltype(xa), decltype(ya)>::value));

    // can automatically assign the underlying enum to the strong enum
    // i.e. xa = va is valid
    STATIC_ASSERT((std::is_assignable<decltype(xa), decltype(va)>::value));

    EXPECT_NO_THROW(xa = V::A);
    EXPECT_EQ(xa, V::A);

    // but not the contrary.
    STATIC_ASSERT((!std::is_assignable<decltype(va), decltype(xa)>::value));

    // extract enum value
    EXPECT_EQ(xa.value(), V::A);

    // and extract underlying type enum
    EXPECT_EQ(xa.underlyingValue(), 1);

    EXPECT_NO_THROW(xb = xa);
    EXPECT_EQ(xa, xb);
}

TEST(UtilsEnum, Assign)
{
    X x;
    x = V::A;

    using XBit = utils::BitAwareEnum<2, X, XTag>;

    XBit construct{V::A};

    XBit xb;
    xb = V::A;
}

