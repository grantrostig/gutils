//
// Created by happycactus on 10/07/18.
//

#ifndef BRIDGEMATE3_SQLITEUTILS_H
#define BRIDGEMATE3_SQLITEUTILS_H

#include <StrongType.h>
#include <boost/optional.hpp>

namespace utils {
template<typename T, typename TAG, typename Validator>
boost::optional<T> make_optional_weaktype(const boost::optional<utils::StrongType<T, TAG, Validator>> &v)
{
    if (v) {
        return boost::optional<T>(v.value().value());
    }
    return boost::optional<T>();
};

template<typename T, typename Q>
boost::optional<T> make_optional_strongtype(boost::optional<Q> v)
{
    if (!v) {
        return boost::none;
    }
    return boost::make_optional(static_cast<T>(v.value()));
}

template<typename T>
boost::optional<std::underlying_type_t<T>> make_optional_underlying(const boost::optional<T> &t)
{
    if (!t) {
        return boost::none;
    }
    return boost::make_optional(static_cast<typename std::underlying_type<T>::type>(t.value()));
}

}

#endif //BRIDGEMATE3_SQLITEUTILS_H
