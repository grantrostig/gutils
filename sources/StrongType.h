/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 01/02/18
 * @addtogroup Bm3Utils
 * @{
 */

#ifndef BRIDGEMATE3_STRONGTYPE_H
#define BRIDGEMATE3_STRONGTYPE_H

#include <utility>
#include <ostream>

namespace utils {

namespace validators {
template<typename T>
struct NoValidator {
    void validate(T const &value)
    {
    }
};

template<typename T, T Min, T Max>
struct MinMaxValidator {
    void validate(T const &value)
    {
        if (value < Min || value > Max) {
            throw std::out_of_range("StrongType Validator doesn't validate");
        }
    }
};
}

/**
 * @brief A Strong Types POD
 * For some usage of this type, @sa PairId, @sa ScoreValue and @sa ScoreTypes.h
 * @tparam T the contained POD type
 * @tparam TAG A Tag for the Strong Types.
 */
template<typename T, typename TAG, typename Validator = utils::validators::NoValidator<T>>
class StrongType : Validator {
private:
    T mValue;

public:
    using Type = T;

    /// @brief the Default Constructor
    StrongType() : mValue()
    {
        Validator::validate(mValue);
    }

    /**
     * @brief Constructor: Creates a StrongType from a POD of type T
     * @param v the value of the POD
     */
    explicit StrongType(T &&v)
            : mValue(std::move(v))
    {
        Validator::validate(mValue);
    }

    explicit StrongType(T const &v)
            : mValue(v)
    {
        Validator::validate(mValue);
    }

    template<typename OtherValidator>
    StrongType(StrongType<T, TAG, OtherValidator> const &t)
    {
        mValue = t.value();
        Validator::validate(mValue);
    }

    template<typename OtherValidator>
    StrongType(StrongType<T, TAG, OtherValidator> &&t)
    {
        mValue = t.value();
        Validator::validate(mValue);
    }

    template<typename OtherValidator>
    StrongType &operator=(StrongType<T, TAG, OtherValidator> const &t)
    {
        mValue = t.value();
        Validator::validate(mValue);
        return *this;
    }

    template<typename OtherValidator>
    StrongType &operator=(StrongType<T, TAG, OtherValidator> &&t)
    {
        mValue = t.value();
        Validator::validate(mValue);
        return *this;
    }

    /**
     * @brief Converts (extracts) the POD value from the StrongType
     * @return the value as a POD (of type T)
     */
    const T &value() const
    { return mValue; }

    T &value()
    { return mValue; }

};

/// @brief Equality operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator==(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() == rh.value());
}

/// @brief Inequality operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator!=(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() != rh.value());
}

/// @brief Less-than operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator<(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() < rh.value());
}

/// @brief Less-than-or-equal-to operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator<=(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() <= rh.value());
}

/// @brief Greater-than operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator>(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() > rh.value());
}

/// @brief Greater-than-or-equal-to operator for StrongType.
template<typename T, typename TAG, typename Validator1, typename Validator2>
inline bool operator>=(StrongType<T, TAG, Validator1> lh, StrongType<T, TAG, Validator2> rh)
{
    return (lh.value() >= rh.value());
}

template<typename T, typename TAG, typename Validator>
std::ostream &operator<<(std::ostream &s, StrongType<T, TAG, Validator> t)
{
    s << t.value();
    return s;
}

/// @brief Addition operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator+(StrongType<T, TAG, Validator> const &l, StrongType<T, TAG, Validator> const &r)
{
    return StrongType<T, TAG, Validator>{l.value() + r.value()};
}

/// @brief Subtraction operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator-(StrongType<T, TAG, Validator> const &l, StrongType<T, TAG, Validator> const &r)
{
    return StrongType<T, TAG, Validator>{l.value() - r.value()};
}

/// @brief Multiplication operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator*(StrongType<T, TAG, Validator> const &l, StrongType<T, TAG, Validator> const &r)
{
    return StrongType<T, TAG, Validator>{l.value() * r.value()};
}

/// @brief Division operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator/(StrongType<T, TAG, Validator> const &l, StrongType<T, TAG, Validator> const &r)
{
    return StrongType<T, TAG, Validator>{l.value() / r.value()};
}

/// @brief Modulo operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator%(StrongType<T, TAG, Validator> const &l, StrongType<T, TAG, Validator> const &r)
{
    return StrongType<T, TAG, Validator>{l.value() % r.value()};
}

/// @brief Unary - operator for Strong Types
template<typename T, typename TAG, typename Validator>
inline StrongType<T, TAG, Validator>
operator-(StrongType<T, TAG, Validator> const &l)
{
    // the following cast is ok because l.value() is of type T.
    return StrongType<T, TAG, Validator>{static_cast<T>(-l.value())};
}

}

/// @}

#endif //BRIDGEMATE3_STRONGTYPE_H
