project(gutils)

set(SOURCES
        os/Daemonizer.cpp os/Daemonizer.h

        fileformats/CsvReader.h

        Factory.h
        DataStream.h
        DataStreamExt.h
        OptionalFields.h
        EndiannesTypes.h
        BitField.h
        BitStream.h
        Enum.h
        BitFieldValidator.h

        StrongType/BitStreamOperations.h
        StrongType/BitFieldValidators.h
        StrongType/BitStreamTypes.h
        Enum/BitStreamOperations.h
        Enum/BitStreamTypes.h
        Enum/BitFieldValidators.h
        )

add_library(gutils SHARED ${SOURCES})

set(LIBRARIES
        Boost::log
        Boost::system
        Boost::thread
        )

set(INCLUDES
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_SOURCE_DIR}/utils
        )

target_include_directories(gutils PRIVATE ${INCLUDES})

target_compile_definitions(gutils PRIVATE BOOST_LOG_USE_NATIVE_SYSLOG)

target_link_libraries(gutils ${LIBRARIES})

install(TARGETS gutils
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT libraries)

