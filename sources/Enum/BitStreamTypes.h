/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_ENUM_BITSTREAMTYPES_H
#define GUTILS_ENUM_BITSTREAMTYPES_H

#include "Enum.h"

namespace utils {

template<int N, typename Type, typename Tag>
struct BitAwareEnum : Enum<Type, Tag> {
    using Enum<Type, Tag>::Enum;


    BitAwareEnum(Type t) : Enum<Type, Tag>(t)
    {}

    BitAwareEnum(BitAwareEnum<N, Type, Tag> const &t) = default;

    BitAwareEnum(BitAwareEnum<N, Type, Tag> &&t) = default;

    BitAwareEnum<N, Type, Tag> &operator=(BitAwareEnum<N, Type, Tag> &&t) = default;

    BitAwareEnum<N, Type, Tag> &operator=(BitAwareEnum<N, Type, Tag> const &t) = default;


    BitAwareEnum<N, Type, Tag> operator=(Type const &o)
    {
        Enum<Type, Tag>::operator=(o);
        return *this;
    }

    BitAwareEnum<N, Type, Tag> operator=(Type &&o)
    {
        Enum<Type, Tag>::operator=(o);
        return *this;
    }
};

}

#endif //GUTILS_BITSTREAMTYPES_H
