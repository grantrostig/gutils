/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 17/01/19
 */

#ifndef GUTILS_ENUM_BITSTREAMVALIDATORS_H
#define GUTILS_ENUM_BITSTREAMVALIDATORS_H

#include "BitFieldValidator.h"

namespace utils {
namespace validators {

template<typename T, typename V, V min, V max>
struct EnumMinMaxValidator {
    void validate(T const &t)
    {
        if (t.underlyingValue() < static_cast<typename std::underlying_type<V>::type>(min) ||
            t.underlyingValue() > static_cast<typename std::underlying_type<V>::type>(max)) {
            throw utils::InBitStream::IllegalValueException("Illegal Enum value");
        }
    }
};

template<typename T, typename V, V Min, V Max, V...Specials>
struct EnumSpecialValidator {
private:
    template<int N>
    void validate_if(T const &t, typename std::enable_if<N < sizeof...(Specials)>::type * = 0)
    {
        if (static_cast<typename std::underlying_type<V>::type>(std::get<N>(std::forward_as_tuple(Specials...))) ==
            t.underlyingValue()) {
            throw utils::InBitStream::IllegalValueException("Illegal Enum value");
        }
        validate_if<N + 1>(t);
    }

    template<int N>
    void validate_if(T const &t, typename std::enable_if<N == sizeof...(Specials)>::type * = 0)
    {
    }

public:
    void validate(T const &t)
    {
        if (t.underlyingValue() < static_cast<typename std::underlying_type<V>::type>(Min) ||
            t.underlyingValue() > static_cast<typename std::underlying_type<V>::type>(Max)) {
            throw utils::InBitStream::IllegalValueException("Illegal Enum value");
        }
        validate_if<0>(t);
    }

};

}
}

#endif //GUTILS_ENUM_BITSTREAMVALIDATORS_H
