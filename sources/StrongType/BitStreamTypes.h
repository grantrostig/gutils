/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/01/19
 */

#ifndef GUTILS_STRONGTYPE_BITSTREAMTYPES_H
#define GUTILS_STRONGTYPE_BITSTREAMTYPES_H

#include "StrongType.h"
#include "BitField.h"

namespace utils {
template<int N, typename Type, typename Tag, typename Validator = validators::NoValidator<Type>>
struct BitAwareStrongType : public StrongType<Type, Tag, Validator> {
    using StrongType<Type, Tag>::StrongType;

    BitAwareStrongType(StrongType<Type, Tag, Validator> t)
            : StrongType<Type, Tag>(t)
    {}
};

template<int N, typename T, typename TAG, typename Validator = validators::NoValidator<T>>
auto makeBitField(utils::StrongType<T, TAG, Validator> var)
{
    return BitField<N, T>(var.value());
}

}

#endif //GUTILS_STRONGTYPE_BITSTREAMTYPES_H
