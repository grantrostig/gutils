/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 17/01/19
 */

#ifndef GUTILS_STRONGTYPE_BITSTREAMVALIDATORS_H
#define GUTILS_STRONGTYPE_BITSTREAMVALIDATORS_H

#include "BitFieldValidator.h"

namespace utils {
namespace validators {

template<typename T, typename V, V min, V max>
struct StrongTypeMinMaxValidator {
    void validate(T const &t)
    {
        if (t.value() < min ||
            t.value() > max) {
            throw utils::InBitStream::IllegalValueException("Illegal XE value");
        }
    }
};
}
}

#endif //GUTILS_STRONGTYPE_BITSTREAMVALIDATORS_H
