/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 16/05/18
 */

#ifndef BRIDGEMATE3_DATASTREAMEXT_H
#define BRIDGEMATE3_DATASTREAMEXT_H

#include "StrongType.h"
#include "DataStream.h"

namespace utils {

namespace details {

template<typename T, typename Endianness>
inline T get_impl(InputDataStream<Endianness> &is, T *)
{
    return is.template get<T>();
};

template<typename T, typename Endianness>
inline void put_impl(OutputDataStream<Endianness> &is, T tp)
{
    is.put(tp);
};


template<typename T, typename TAG, typename Endianness, typename Validator>
inline StrongType<T, TAG> get_impl(InputDataStream<Endianness> &is, StrongType<T, TAG, Validator> *)
{
    return StrongType<T, TAG>(is.template get<typename StrongType<T, TAG, Validator>::Type>());
};

template<typename T, typename TAG, typename Endianness, typename Validator>
inline void put_impl(OutputDataStream<Endianness> &is, StrongType<T, TAG, Validator> tp)
{
    is.put(tp.value());
};

template<typename T, typename Endianness>
inline std::vector<T> get_impl(InputDataStream<Endianness> &is, std::vector<T> *)
{
    std::vector<T> r;
    uint8_t n = is.template get<uint8_t>();
    for (int i = 0; i < n; ++i) {
        r.emplace_back(get_impl(is, static_cast<T *>(nullptr)));
    }
    return r;
};

template<typename T, typename Endianness>
inline void put_impl(OutputDataStream<Endianness> &os, std::vector<T> d)
{
    os.template put<uint8_t>(static_cast<uint8_t>(d.size()));
    for (const auto &i : d) {
        put_impl(os, i);
    }
};


template<typename TAG, typename Validator, typename Endianness, typename StringSize = uint8_t>
inline StrongType<std::string, TAG, Validator>
get_impl(InputDataStream<Endianness> &is, StrongType<std::string, TAG, Validator> *)
{
    return StrongType<std::string, TAG, Validator>(is.template getString<StringSize>());
};

template<typename TAG, typename Validator, typename Endianness, typename StringSize = uint8_t>
inline void put_impl(OutputDataStream<Endianness> &is, StrongType<std::string, TAG, Validator> tp)
{
    is.template putString<StringSize>(tp.value());
};

template<typename Endianness>
inline std::string get_impl(InputDataStream<Endianness> &is, std::string *)
{
    return is.template getString<uint8_t>();
};

template<typename Endianness>
inline void put_impl(OutputDataStream<Endianness> &is, std::string string)
{
    is.template putString<uint8_t>(string);
};

template<typename Endianness>
inline bool get_impl(InputDataStream<Endianness> &is, bool *)
{
    return (is.template get<uint8_t>()) != 0;
};

template<typename Endianness>
inline void put_impl(OutputDataStream<Endianness> &is, bool v)
{
    is.template put<uint8_t>(v ? 1 : 0);
};


}

template<typename T, typename Endianness>
[[deprecated]]
inline T get(InputDataStream<Endianness> &is)
{
    return details::get_impl(is, static_cast<T *>(nullptr));
};

template<typename T, typename Endianness>
[[deprecated]]
inline void put(OutputDataStream<Endianness> &is, T v)
{
    details::put_impl(is, v);
};

}

#endif //BRIDGEMATE3_DATASTREAMEXT_H
